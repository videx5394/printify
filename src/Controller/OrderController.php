<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\User;
use App\Entity\UserProduct;
use App\Service\OrderService;
use App\Service\UserService;
use App\Utils\ErrorMessage;
use App\Utils\Validator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderController extends AbstractController
{
    /**
     * @Route("/orders", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function placeOrder(Request $request, ValidatorInterface $validator): Response
    {
        $userId = (int)$request->get('userId');
        $shipping = $request->get('shipping');
        $shippingType = $request->get('shippingType');
        $zip = $request->get('zip');
        $state = $request->get('state');

        $order = new Order();
        $order->setFullName($request->get('fullName'));
        $order->setAddress($request->get('address'));
        $order->setCountry($request->get('country'));
        $order->setShipping($shipping);
        $order->setShippingType($shippingType);
        $order->setState($state);
        $order->setZip($zip);
        $order->setCity($request->get('city'));
        $order->setPhone($request->get('phone'));
        $order->setRegion($request->get('region'));

        $errors = $validator->validate($order);

        if (count($errors) > 0) {
            return $this->json($errors, 400);
        }

        if ($userId === 0) {
            return $this->json(new ErrorMessage("Invalid userId"), 400);
        }

        $userRepo = $this->getDoctrine()->getRepository(User::class);
        /**
         * @var $foundUser User
         */
        $foundUser = $userRepo->find($userId);

        if (empty($foundUser)) {
            return $this->json(new ErrorMessage("User not found"), 400);
        }

        $userProductRepo = $this->getDoctrine()->getRepository(UserProduct::class);

        $userProducts = $userProductRepo->findProductsByUserId($userId);

        if (empty($userProducts)) {
            return $this->json(new ErrorMessage("Product list is empty"), 400);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->getConnection()->beginTransaction();
        try {
            $readyOrder = OrderService::prepareToPlaceOrder($order, $foundUser, $userProducts);
            $totalCost = $readyOrder->getTotalCost();
            $foundUser = UserService::updateUserBalance($foundUser, $totalCost);

            $entityManager->persist($readyOrder);
            $entityManager->persist($foundUser);
            //remove products from product list
            foreach ($userProducts as $userProduct) {
                $entityManager->remove($userProduct);
            }
            $entityManager->flush();
            $entityManager->getConnection()->commit();
        } catch (\Exception $exception) {
            $entityManager->getConnection()->rollBack();
            return $this->json(new ErrorMessage($exception->getMessage()), 400);
        }
        return new Response(null, 201);
    }
}
