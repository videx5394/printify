<?php


namespace App\Service;


use App\Entity\Order;
use App\Entity\User;
use App\Entity\UserProduct;
use Doctrine\Common\Collections\Collection;
use Laminas\EventManager\Exception\RuntimeException;

class OrderService
{
    /**
     * @param Order $order
     * @param User $user
     * @param UserProduct[] $userProducts
     * @return Order
     */
    public static function prepareToPlaceOrder(Order $order, User $user,array $userProducts) : Order
    {
        foreach ($userProducts as $userProduct)
        {
            $order->addProduct($userProduct->getProduct());
        }
        $order->setUser($user);

        $totalCost = self::calculateTotalCost($userProducts,$order->getShipping(),$order->getShippingType());

        $order->setTotalCost($totalCost);

        return $order;
    }

    /**
     * @param UserProduct[] $userProducts
     * @param $shipping
     * @param $shippingType
     * @return float
     */
    public static function calculateTotalCost(array $userProducts,$shipping,$shippingType) : float
    {
        $totalCost = 0;
        $standardShippingCosts = Order::getStandardShippingCosts();
        $expressShippingCost = Order::getExpressShippingCost();

        if($shipping === 'express' && $shippingType != 'domestic')
            throw new \RuntimeException("Only domestic orders available in express shipping");

        foreach ($userProducts as $key => $userProduct) {
            $shippingCost = 0;
            $product = $userProduct->getProduct();
            $quantity = $userProduct->getQuantity();

            if($shipping === 'standard')
            {
                for ($i=0;$i<$quantity;$i++)
                {
                    $sequence = 'first';
                    if($i!=0)
                        $sequence = 'subsequent';
                    $shippingCost = $standardShippingCosts[$product->getProductType()][$shippingType][$sequence];
                    $totalCost += $product->getCost() + $shippingCost;
                }
            }
            else if($shipping === 'express')
            {
                for ($i=0;$i<$quantity;$i++)
                {
                    $shippingCost += $expressShippingCost;
                    $totalCost += $product->getCost() + $shippingCost;
                }
            }
        }
        return $totalCost;
    }
}