<?php

namespace App\Tests\Controller;

use App\Controller\UserController;
use App\Repository\UserRepository;
use Faker\Factory;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    use FixturesTrait;

    private $client;
    public function setUp():void
    {
        $this->client = $this->createClient();
        $this->loadFixtures(array(
            'App\DataFixtures\AppFixtures'
        ));
    }

    public function testCreateUser()
    {
        $faker = Factory::create();

        $email = $faker->email;
        $username = $faker->userName;

        $userRepository = static::$container->get(UserRepository::class);

        $checkEmail = $userRepository->findByEmail($email);
        $checkUsername = $userRepository->findByUsername($username);

        $this->client->request('POST', 'users', [
            'email' => $email,
            'username' => $username,
            'password' => '123456'
        ]);

        if (!empty($checkEmail) || !empty($checkUsername)) {
            $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        } else {
            $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        }
    }
}
