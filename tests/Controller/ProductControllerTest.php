<?php

namespace App\Tests\Controller;

use App\Controller\ProductController;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Faker\Factory;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{

    use FixturesTrait;

    private $client;
    public function setUp():void
    {
        $this->client = $this->createClient();
        $this->loadFixtures(array(
            'App\DataFixtures\AppFixtures'
        ));
    }


    public function testAddProduct()
    {
        $faker = Factory::create();

        $scu = $faker->randomDigit;

        $productRepository = static::$container->get(ProductRepository::class);

        $checkScu = $productRepository->findByScu($scu);

        $this->client->request('POST', 'products', [
            'title' => $faker->title,
            'scu' => $scu,
            'cost' => $faker->randomFloat(2,1,20),
            'productType' => $faker->randomElement(['mug','t-shirt'])
        ]);

        if (!empty($checkScu)) {
            $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        } else {
            $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        }

    }

    public function testGetProductsUserNotFound()
    {

        $this->client->request('GET', 'products/0');
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testGetProducts()
    {

        $userRepository = static::$container->get(UserRepository::class);

        /**
         * @var $user User
         */
        $user =  $userRepository->findOneBy([]);

        $userId = !empty($user) ? $user->getId() : 0;

        $this->client->request('GET', 'products/'.$userId);

        if(empty($user))
        {
            $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        }
        else
        {
            $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        }
    }
}
