<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * * @UniqueEntity(
 *     fields={"scu"}
 * )
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $scu;

    /**
     * @Assert\NotBlank
     * @Assert\PositiveOrZero
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $cost;

    /**
     * @Assert\NotBlank
     * @Assert\Choice(
     *     choices = { "mug", "t-shirt" },
     *     message = "only two types of products can be created - mugs and t-shirts."
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $productType;

    /**
     * @ORM\OneToMany(targetEntity=UserProduct::class, mappedBy="product" )
     */
    private $userProducts;

    public function __construct()
    {
        $this->userProducts = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getScu(): ?string
    {
        return $this->scu;
    }

    public function setScu(?string $scu): self
    {
        $this->scu = $scu;

        return $this;
    }

    public function getCost(): ?string
    {
        return $this->cost;
    }

    public function setCost(?string $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getProductType(): ?string
    {
        return $this->productType;
    }

    public function setProductType(?string $productType): self
    {
        $this->productType = $productType;

        return $this;
    }

}
