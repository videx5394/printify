<?php

namespace App\Tests\Controller;

use App\Controller\UserProductController;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Faker\Factory;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserProductControllerTest extends WebTestCase
{

    use FixturesTrait;

    private $client;
    public function setUp():void
    {
        $this->client = $this->createClient();
        $this->loadFixtures(array(
            'App\DataFixtures\AppFixtures'
        ));
    }

    public function testAddProduct()
    {

        $userRepository = static::$container->get(UserRepository::class);
        /**
         * @var $user User
         */
        $user =  $userRepository->findOneBy([]);
        $userId = $user->getId();

        $productRepository = static::$container->get(ProductRepository::class);
        /**
         * @var $product Product
         */
        $product =  $productRepository->findOneBy([]);
        $productId = $product->getId();

        $this->client->request('POST', "/user-products/$userId/$productId/1");

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
    }

    public function testUserNotFoundWhenAddProduct()
    {
        $productRepository = static::$container->get(ProductRepository::class);
        /**
         * @var $product Product
         */
        $product =  $productRepository->findOneBy([]);
        $productId = $product->getId();
        $this->client->request('POST', "/user-products/0/$productId/1");
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testProductNotFoundWhenAddProduct()
    {
        $userRepository = static::$container->get(UserRepository::class);
        /**
         * @var $user User
         */
        $user =  $userRepository->findOneBy([]);
        $userId = $user->getId();
        $this->client->request('POST', "/user-products/$userId/0/1");
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

}
