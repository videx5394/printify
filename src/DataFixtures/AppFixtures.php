<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\User;
use App\Entity\UserProduct;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AppFixtures extends Fixture
{
    /** @var ObjectManager */
    private $manager;
    /** @var Generator */
    protected $faker;

    private $products = [];
    private $users = [];


    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->faker = Factory::create();
        $this->loadData();
        //$this->loadPosts();
    }

    private static $productTypes = [
        'mug',
        't-shirt'
    ];

    public function loadData()
    {
        $this->loadPosts();
        $this->loadUsers();
        $this->loadUserPosts();
    }

    private function loadPosts(): void
    {
        foreach (range(1, 3) as $i) {
            $product = new Product();
            $product->setTitle($this->faker->title());
            $product->setScu($this->faker->unique()->randomDigit);
            $product->setCost($this->faker->randomFloat(2,1,20));
            $product->setProductType($this->faker->randomElement(self::$productTypes));
            $this->manager->persist($product);
            $this->products[] = $product;
        }
        $this->manager->flush();
    }

    private function loadUsers(): void
    {
        foreach (range(1, 2) as $i) {
            $user = new User();
            $user->setBalance(100);
            $user->setUsername($this->faker->unique()->userName);
            $user->setPassword($this->faker->password);
            $user->setEmail($this->faker->unique()->email);
            $this->manager->persist($user);
            $this->users[] = $user;
        }
        $this->manager->flush();
    }

    private function loadUserPosts(): void
    {
        foreach ($this->users as $user)
        {
            foreach ($this->products as $product)
            {
                $userProduct = new UserProduct();
                $userProduct->setUser($user);
                $userProduct->setProduct($product);
                $userProduct->setQuantity($this->faker->randomElement([1, 2,3]));
                $this->manager->persist($userProduct);
            }
        }
        $this->manager->flush();
    }

}
