<?php

namespace App\Tests\Service;

use App\Entity\Product;
use App\Entity\User;
use App\Entity\UserProduct;
use App\Service\OrderService;
use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class OrderServiceTest extends TestCase
{
    private $user;
    private $userProducts = [];
    /** @var Generator */
    protected $faker;

    public function setUp() :void
    {
        $this->faker = Factory::create();
        $user = new User();
        $user->setBalance(100);
        $user->setUsername($this->faker->unique()->userName);
        $user->setPassword($this->faker->password);
        $user->setEmail($this->faker->unique()->email);

        $this->user = $user;

        $productTypes = [
            'mug',
            't-shirt'
        ];

        $quantities = [1,2];

        foreach (range(0, 1) as $i) {
            $product = new Product();
            $product->setTitle($this->faker->title());
            $product->setScu($this->faker->unique()->randomDigit);
            $product->setCost(($i+1)*10);
            $product->setProductType($productTypes[$i]);
            $userProduct = new UserProduct();
            $userProduct->setUser($user);
            $userProduct->setProduct($product);
            $userProduct->setQuantity($quantities[$i]);
            $this->userProducts[] = $userProduct;
        }
    }

    public function testCalculateTotalCost()
    {
        $totalCost = OrderService::calculateTotalCost($this->userProducts,'standard','domestic');

        $this->assertEquals($totalCost,53.5);
    }

    public function testNotAllowCalculateExpressInternationalOrders()
    {

        $this->expectException(\RuntimeException::class);

        OrderService::calculateTotalCost($this->userProducts,'express','international');
    }


}
