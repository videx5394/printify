<?php


namespace App\Service;


use App\Entity\User;

class UserService
{
    public static function updateUserBalance(User $user, int $totalCost): User
    {
        if ($totalCost > $user->getBalance()) {
            throw new \RuntimeException("Insufficient funds");
        }
        $updatedBalance = $user->getBalance() - $totalCost;
        $user->setBalance($updatedBalance);

        return  $user;
    }
}