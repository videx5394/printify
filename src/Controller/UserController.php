<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\User;
use App\Utils\ErrorMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{

    /**
     * @Route("/users", name="createUser", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function createUser( Request $request,ValidatorInterface $validator,UserPasswordEncoderInterface $encoder) : Response
    {
        $user = new User();
        $user->setEmail($request->get('email'));
        $user->setUsername($request->get('username'));
        $user->setPassword($request->get('password'));

        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            return $this->json($errors,400);
        }

        //encode password
        $encoded = $encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($encoded);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return new Response(null,201);
    }
}
