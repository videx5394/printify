<?php

namespace App\Tests\Controller;

use App\Controller\OrderController;
use App\DataFixtures\AppFixtures;
use App\Entity\User;
use App\Repository\UserRepository;
use Faker\Factory;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
Use Doctrine\Common\DataFixtures\Purger\ORMPurger;

/**
 * @testtype Functional
 */
class OrderControllerTest extends WebTestCase
{

    use FixturesTrait;

    private $client;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    private function getPlaceOrderData($userId)
    {
        $faker = Factory::create();
        return [
            'userId' => $userId,
            'fullName' => $faker->name,
            'address' => $faker->address,
            'shipping' => 'standard',
            'shippingType' => 'domestic',
            'zip' => $faker->postcode,
            'state' => $faker->state,
            'city' => $faker->city,
            'phone' => $faker->phoneNumber,
            'region' => $faker->streetAddress,
            'country' => $faker->country
        ];
    }

    public function setUp():void
    {
        $this->client = $this->createClient();
        $this->loadFixtures(array(
            'App\DataFixtures\AppFixtures'
        ));
        $this->entityManager = $this->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testPlaceOrder()
    {
        $userRepository = static::$container->get(UserRepository::class);
        /**
         * @var $user User
         */
        $user =  $userRepository->findOneBy([]);
        $userId = $user->getId();

        $data = $this->getPlaceOrderData($userId);

        $this->client->request('POST', 'orders', $data);
        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
    }

    public function testPlaceOrderWithNonExistingUser()
    {
        $userRepository = static::$container->get(UserRepository::class);
        /**
         * @var $user User
         */
        $user =  $userRepository->findOneBy([],['id'=>'DESC']);
        $userId = $user->getId()+1;

        $data = $this->getPlaceOrderData($userId);
        $this->client->request('POST', 'orders', $data);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }


    public function testPlaceOrderWithProductListIsEmpty()
    {
        $faker = Factory::create();

        $userRepository = static::$container->get(UserRepository::class);
        /**
         * @var $user User
         */
        $user = new User();
        $user->setEmail($faker->email);
        $user->setUsername($faker->userName);
        $user->setPassword($faker->password);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $data = $this->getPlaceOrderData($user->getId());
        $this->client->request('POST', 'orders', $data);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }
}
