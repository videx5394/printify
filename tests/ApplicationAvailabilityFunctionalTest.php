<?php


namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApplicationAvailabilityFunctionalTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     * @param $url
     */
    public function testPageIsSuccessful($method,$url)
    {
        $client = self::createClient();
        $client->request($method, $url);

        $this->assertTrue(!$client->getResponse()->isNotFound());
    }

    public function urlProvider()
    {
        yield ['POST','users'];
        yield ['POST','/products'];
        yield ['GET','products/1'];
        yield ['POST','user-products/1/1/1'];
        yield ['POST','/orders'];
    }
}