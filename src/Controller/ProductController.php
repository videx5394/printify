<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\User;
use App\Entity\UserProduct;
use App\Utils\ErrorMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductController extends AbstractController
{

    /**
     * @Route("/products", name="addProduct", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function addProduct(Request $request,ValidatorInterface $validator): Response
    {
        $product = new Product();
        $product->setProductType($request->get('productType'));
        $product->setCost($request->get('cost'));
        $product->setScu($request->get('scu'));
        $product->setTitle($request->get('title'));

        $errors = $validator->validate($product);

        if (count($errors) > 0) {
            return $this->json($errors,400);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();

        return new Response(null,201);
    }

    /**
     * @Route("/products/{userId}", methods={"GET"}, requirements={"userId"="\d+"})
     * @param int $userId
     * @return Response
     */
    public function getProducts(int $userId): Response
    {
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $foundUser = $userRepo->find($userId);

        if (is_null($foundUser)) {
            return $this->json(new ErrorMessage("User not found"), 400);
        }

        $userProductRepo = $this->getDoctrine()->getRepository(UserProduct::class);

        $userProducts = $userProductRepo->findProductsByUserId($userId);

        $products = [];
        foreach ($userProducts as $userProduct)
        {
            $product = $userProduct->getProduct();
            $products[] = [
                'title' => $product->getTitle(),
                'scu' => $product->getScu(),
                'cost' => $product->getCost(),
                'productType' => $product->getProductType(),
                'quantity' => $userProduct->getQuantity(),
            ];
        }
        return $this->json($products);
    }
}
