Printify RESTful API
========================

The "Printify RESTful API" is a  RESTful API where users can create products and place orders with those products.

Requirements
------------

  * Git
  * Docker
  * Docker-compose

Installation
------------

First, clone this repository:

```bash
$ git clone https://videx5394@bitbucket.org/videx5394/printify.git
```

Usage
-----

There's no need to configure anything to run the application. 

Just run following:

```bash
$ cd printify/
$ docker-compose up
```

Then access the application in your browser at the given URL (<http://127.0.0.1:8080> by default). 


Please checkout the shared [Postman collection][1] to get available API list. 


There are 3 available docker containers:

### PHP (PHP-FPM)
```
$ docker exec -it printify_php-fpm_1 bash
```

### Database (MariaDB)
```
$ docker exec -it printify_db_1 mysql -u root -p
$ root
```

### Webserver (Nginx)
```
$ docker exec -it printify_nginx_1 bash
```


Tests
-----

Execute this commands to run tests:

```bash
$ docker exec -it printify_php-fpm_1 bash
$ php bin/phpunit
```

[1]: https://documenter.getpostman.com/view/3246101/T17Nbjks?version=latest