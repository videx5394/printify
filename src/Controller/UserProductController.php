<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\User;
use App\Entity\UserProduct;
use App\Utils\ErrorMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserProductController extends AbstractController
{


    /**
     * @Route("/user-products/{userId}/{productId}/{quantity}", methods={"POST"}, requirements={"userId"="\d+","productId"="\d+","quantity"="\d+"})
     * @param int $userId
     * @param int $productId
     * @param int $quantity
     * @return Response
     */
    public function addProduct(int $userId, int $productId,int $quantity = 1) : Response
    {
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        /**
         * @var $foundUser User
         */
        $foundUser = $userRepo->find($userId);

        $productRepo = $this->getDoctrine()->getRepository(Product::class);
        /**
         * @var $foundProduct Product
         */
        $foundProduct = $productRepo->find($productId);

        $userProductRepo = $this->getDoctrine()->getRepository(UserProduct::class);

        if (is_null($foundUser)) {
            return $this->json(new ErrorMessage("User not found"), 400);
        }

        if(is_null($foundProduct)){
            return $this->json(new ErrorMessage("Product not found"), 400);
        }

        $userProduct = new UserProduct();
        $userProduct->setProduct($foundProduct);
        $userProduct->setUser($foundUser);
        $userProduct->setQuantity($quantity);

        $entityManager = $this->getDoctrine()->getManager();

        //remove previous product of user
        $prevUserProduct = $userProductRepo->findOneBy(['user' => $foundUser, 'product' => $foundProduct]);
        if(!is_null($prevUserProduct))
        {
            $entityManager->remove($prevUserProduct);
            $entityManager->flush();
        }


        $entityManager->persist($userProduct);
        $entityManager->flush();

        return new Response(null,201);
    }

}
