<?php

namespace App\Tests\Service;

use App\Entity\User;
use App\Service\UserService;
use PHPUnit\Framework\TestCase;

class UserServiceTest extends TestCase
{
    public function testUpdateUserBalance()
    {
        $user = new User();
        $user->setBalance(100);
        UserService::updateUserBalance($user,40);

        $this->assertEquals($user->getBalance(),60);
    }

    public function testInsufficientFundsWhenUpdateUserBalance()
    {
        $user = new User();
        $user->setBalance(10);

        $this->expectException(\RuntimeException::class);

        UserService::updateUserBalance($user,20);
    }


}
