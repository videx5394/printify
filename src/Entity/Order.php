<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $fullName;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=50)
     */
    private $phone;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class)
     */
    private $product;

    /**
     * @Assert\NotBlank
     * @Assert\Choice(
     *     choices = { "standard", "express" },
     *     message = "Invalid shipping method"
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $shipping;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Choice(
     *     choices = { "international", "domestic" },
     *     message = "Invalid shipping type"
     * )
     */
    private $shippingType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $totalCost;


    private const STANDARD_SHIPPING_COSTS = [
        'mug' => [
            'domestic'=> [
                'first' => 2,
                'subsequent' => 1
            ],
            'international' => [
                'first' => 5,
                'subsequent' => 2.5
            ]
        ],
        't-shirt' => [
            'domestic'=> [
                'first' => 1,
                'subsequent' => 0.5
            ],
            'international' => [
                'first' => 3,
                'subsequent' => 1.5
            ]
        ]
    ];

    private const EXPRESS_SHIPPING_COST = 10;

    public function __construct()
    {
        $this->product = new ArrayCollection();
    }


    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validate(ExecutionContextInterface $context, $payload) : void
    {

        if($this->getShippingType() === 'domestic')
        {
            if(empty($this->getZip()))
            {
                $context->buildViolation('ZIP should not be empty in domestic orders')
                    ->atPath('zip')
                    ->addViolation();
            }
            if(empty($this->getState()))
            {
                $context->buildViolation('State should not be empty in domestic orders')
                    ->atPath('state')
                    ->addViolation();
            }
        }
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(?string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
        }

        return $this;
    }

    public function getShipping(): ?string
    {
        return $this->shipping;
    }

    public function setShipping(?string $shipping): self
    {
        $this->shipping = $shipping;

        return $this;
    }

    public function getShippingType(): ?string
    {
        return $this->shippingType;
    }

    public function setShippingType(?string $shippingType): self
    {
        $this->shippingType = $shippingType;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getTotalCost(): ?string
    {
        return $this->totalCost;
    }

    public function setTotalCost(string $totalCost): self
    {
        $this->totalCost = $totalCost;

        return $this;
    }

    public static function getStandardShippingCosts(): ?array
    {
        return self::STANDARD_SHIPPING_COSTS;
    }

    public static function getExpressShippingCost(): ?int
    {
        return self::EXPRESS_SHIPPING_COST;
    }
}
