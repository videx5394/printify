<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\User;
use App\Entity\UserProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserProduct[]    findAll()
 * @method UserProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserProduct::class);
    }

    public function findProductsByUserId($userId)
    {
        $result = $this->createQueryBuilder('up')
            ->select('up')
            ->andWhere('up.user = :val')
            ->setParameter('val', $userId)
            ->leftJoin('up.product', 'p')
            ->getQuery()
            ->getResult();

        return $result;
    }
}
